# Team members info:

| MSSV     |    Ho Va Ten     | So Dien Thoai |        Mail Sinh Vien         | Mail Ca Nhan            |
| -------- | :--------------: | ------------: | :---------------------------: | ----------------------- |
| 20120511 | Nguyen Quoc Khoa |    0969486906 | 20120511@student.hcmus.edu.vn | khoanguyen517@gmail.com |
| 20120529 |    Huynh Luat    |    0794515902 | 20120529@student.hcmus.edu.vn | huynhluat.dn@gmail.com  |
| 20120508 | Huynh Tan Khanh  |    0779585825 | 20120508@student.hcmus.edu.vn | htk02102002@gmail.com   |

# Login to docker Hub

Since our docker images are published to free-tier docker hub, which is a private registry. We need to login to the docker hub via the username and token, in order to pull the image to the local machine.

`docker login -u khoanguyen517@gmail.com -p dckr_pat__gpkhRIXPp3ODwxh72uCkwA2JA0`

# How to run

After logged in to the docker hub. We then use the docker compose up command to run all the services.

`docker compose up`

Or if you want to run in detached mode then add -d flag to the command:

`docker compose up -d`
